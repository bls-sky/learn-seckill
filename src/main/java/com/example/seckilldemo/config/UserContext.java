package com.example.seckilldemo.config;

import com.example.seckilldemo.pojo.User;

/**
 * TODO
 *
 * @author 59244
 * @date 2021/11/28 16:58
 */
public class UserContext {
    private static ThreadLocal<User> userThreadLocal = new ThreadLocal<>();

    public static void setUser(User user){
        userThreadLocal.set(user);
    }

    public static User getUser(){
        return userThreadLocal.get();
    }
}
