package com.example.seckilldemo.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * TODO rabbitmq配置类
 *
 * @author 59244
 * @date 2021/11/27 10:39
 */
@Configuration
public class RabbitMQConfig {

    private static final String QUEUE = "seckill_queue";
    private static final String EXCHANGE = "topicExchange";
    private static final String ROUTINGKEY = "seckill.#";

    @Bean
    public Queue queue(){
        return new Queue(QUEUE);
    }
    @Bean
    public TopicExchange topicExchange(){
        return new TopicExchange(EXCHANGE);
    }
    @Bean
    public Binding binding(){
        return BindingBuilder.bind(queue()).to(topicExchange()).with(ROUTINGKEY);
    }



//    private static final String QUEUE_FANOUT01 = "queue_fanout01";
//    private static final String QUEUE_FANOUT02 = "queue_fanout02";
//    private static final String EXCHANGE_FANOUT = "fanoutExchange";
//
//
//    @Bean
//    public Queue queue(){
//        return new Queue("queue",true);
//    }
//
//    /**
//     * 功能描述：测试fanout模式的配置类
//     *
//     * @params:
//     * @return：
//     * @author: 59244
//     * @time: 2021/11/27 13:38
//     */
//    @Bean
//    public Queue queueFanout01(){
//        return new Queue(QUEUE_FANOUT01);
//    }
//    @Bean
//    public Queue queueFanout02(){
//        return new Queue(QUEUE_FANOUT02);
//    }
//    @Bean
//    public FanoutExchange fanoutExchange(){
//        return new FanoutExchange(EXCHANGE_FANOUT);
//    }
//    @Bean
//    public Binding bindingFanout01(){
//        return BindingBuilder.bind(queueFanout01()).to(fanoutExchange());
//    }
//    @Bean
//    public Binding bindingFanout02(){
//        return BindingBuilder.bind(queueFanout02()).to(fanoutExchange());
//    }
//
//
//    private static final String QUEUE_DIRECT01 = "queue_direct01";
//    private static final String QUEUE_DIRECT02 = "queue_direct02";
//    private static final String EXCHANGE_DIRECT = "directExchange";
//    private static final String ROUTINGKEY01 = "queue.red";
//    private static final String ROUTINGKEY02 = "queue.green";
//    /**
//     * 功能描述：测试direct模式的配置类
//     *
//     * @params:
//     * @return：
//     * @author: 59244
//     * @time: 2021/11/27 13:38
//     */
//    @Bean
//    public Queue queueDirect01(){
//        return new Queue(QUEUE_DIRECT01);
//    }
//    @Bean
//    public Queue queueDirect02(){
//        return new Queue(QUEUE_DIRECT02);
//    }
//    @Bean
//    public DirectExchange directExchange(){
//        return new DirectExchange(EXCHANGE_DIRECT);
//    }
//    @Bean
//    public Binding bindingDirect01(){
//        return BindingBuilder.bind(queueDirect01()).to(directExchange()).with(ROUTINGKEY01);
//    }
//    @Bean
//    public Binding bindingDirect02(){
//        return BindingBuilder.bind(queueDirect01()).to(directExchange()).with(ROUTINGKEY02);
//    }
//
//
//    private static final String QUEUE_TOPIC01 = "queue_topic01";
//    private static final String QUEUE_TOPIC02 = "queue_topic02";
//    private static final String EXCHANGE_TOPIC = "topicExchange";
//    private static final String ROUTINGKEY_TOPIC01 = "#.queue.#";
//    private static final String ROUTINGKEY_TOPIC02 = "*.queue.#";
//    /**
//     * 功能描述：测试topic模式的测试类
//     *
//     * @params:
//     * @return：
//     * @author: 59244
//     * @time: 2021/11/27 13:42
//     */
//    @Bean
//    public Queue queueTopic01(){
//        return new Queue(QUEUE_TOPIC01);
//    }
//    @Bean
//    public Queue queueTopic02(){
//        return new Queue(QUEUE_TOPIC02);
//    }
//    @Bean
//    public TopicExchange topicExchange(){
//        return new TopicExchange(EXCHANGE_TOPIC);
//    }
//    @Bean
//    public Binding bindingTopic01(){
//        return BindingBuilder.bind(queueTopic01()).to(topicExchange()).with(ROUTINGKEY_TOPIC01);
//    }
//    @Bean
//    public Binding bindingTopic02(){
//        return BindingBuilder.bind(queueTopic02()).to(topicExchange()).with(ROUTINGKEY_TOPIC02);
//    }
}
