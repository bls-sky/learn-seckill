package com.example.seckilldemo.controller;


import com.example.seckilldemo.pojo.User;
import com.example.seckilldemo.rabbitmq.MQSeander;
import com.example.seckilldemo.vo.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sean
 * @since 2021-11-14
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    MQSeander mqSeander;


    @RequestMapping("/info")
    @ResponseBody
    public RespBean info(User user){
        return RespBean.success(user);
    }


    /**
     * 功能描述：测试发送rabbitmq
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 11:00
     */
    @RequestMapping("/mq")
    @ResponseBody
    public void mq(){
        mqSeander.send("hello");
    }

    /**
     * 功能描述：Fanout模式
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 11:51
     */
    @RequestMapping("/mq/fanout")
    @ResponseBody
    public void fanout(){
        mqSeander.sendFanout("hello");
    }

    /**
     * 功能描述：direct模式
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:41
     */
    @RequestMapping("/mq/direct01")
    @ResponseBody
    public void direct01(){
        mqSeander.sandDirectRed("hello,red");
    }
    @RequestMapping("/mq/direct02")
    @ResponseBody
    public void direct02(){
        mqSeander.sandDirectGreen("hello,green");
    }

    /**
     * 功能描述：topic模式
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 13:54
     */
    @RequestMapping("/mq/topic01")
    @ResponseBody
    public void topic01(){
        mqSeander.sendTopicOne("hello,one bro");
    }
    @RequestMapping("/mq/topic02")
    @ResponseBody
    public void topic02(){
        mqSeander.sendTopicTwo("hello,Two bro");
    }
}
