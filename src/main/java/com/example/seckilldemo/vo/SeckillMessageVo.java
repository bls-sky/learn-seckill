package com.example.seckilldemo.vo;

import com.example.seckilldemo.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO 秒杀消息类  用于传递给rabbitmq，然后异步生成订单
 *
 * @author 59244
 * @date 2021/11/27 15:30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SeckillMessageVo {
    private User user;
    private Long goodsId;
}
