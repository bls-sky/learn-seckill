package com.example.seckilldemo.utils;

import java.util.UUID;

/**
 * TODO UUID工具类
 *
 * @author 59244
 * @date 2021/11/14 21:26
 */
public class UUIDUtil {
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
