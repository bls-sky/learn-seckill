package com.example.seckilldemo.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * TODO MD5工具类
 *
 * @author 59244
 * @date 2021/11/14 21:26
 */
public class MD5Util {
    public static String md5(String str){
        return DigestUtils.md5Hex(str);
    }

    private static String salt = "1a2b3c4d5e";//和前端对用的盐

    public static String inputPassToFormPass(String inputPass){
        String str = "" + salt.charAt(0) + salt.charAt(2) + inputPass + salt.charAt(7) + salt.charAt(9);
        return md5(str);
    }

    public static String formPassToDBPass(String formPass, String salt){//这个salt用的时候随机给，存到数据库里的(不太理解这里：这个salt存到数据库，那数据库被盗就可以利用这个salt破解后端生成的密码)
        String str = "" + salt.charAt(0) + salt.charAt(2) + formPass + salt.charAt(7) + salt.charAt(9);
        return md5(str);
    }

    public static String inputPassToDBPass(String inputPass, String salt){
        String formPass = inputPassToFormPass(inputPass);
        String dbPass = formPassToDBPass(formPass, salt);
        return dbPass;
    }

    public static void main(String[] args) {
        System.out.println(inputPassToFormPass("123456"));//4349aa669e70d716217998703346baf4
        System.out.println(formPassToDBPass("4349aa669e70d716217998703346baf4","1a2b3c4d5e"));
        System.out.println(inputPassToDBPass("123456","1a2b3c4d5e"));
    }
}
