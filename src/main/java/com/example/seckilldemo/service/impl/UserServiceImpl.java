package com.example.seckilldemo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.seckilldemo.exception.GlobalException;
import com.example.seckilldemo.mapper.UserMapper;
import com.example.seckilldemo.pojo.User;
import com.example.seckilldemo.service.IUserService;
import com.example.seckilldemo.utils.CookieUtil;
import com.example.seckilldemo.utils.MD5Util;
import com.example.seckilldemo.utils.UUIDUtil;
import com.example.seckilldemo.vo.LoginVo;
import com.example.seckilldemo.vo.RespBean;
import com.example.seckilldemo.vo.RespBeanEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sean
 * @since 2021-11-14
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 功能描述：登录
     *
     * @params: 
     * @return： 
     * @author: 59244
     * @time: 2021/11/14 18:47
     */
    @Override
    public RespBean dologin(LoginVo loginVo, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String mobile = loginVo.getMobile();
        String password = loginVo.getPassword();
        //参数校验
//        if(StringUtils.isEmpty(mobile) && StringUtils.isEmpty(password)){
//            return RespBean.error(RespBeanEnum.LOGIN_ERROR);
//        }
//        if(!ValidatorUtil.isMobile(mobile)){
//            return RespBean.error(RespBeanEnum.MOBILE_ERROR);
//        }
        //通过手机号查询密码
        User user = userMapper.selectById(mobile);
        if(user == null){
            throw new GlobalException(RespBeanEnum.LOGIN_ERROR);
        }
        if(!user.getPassword().equals(MD5Util.formPassToDBPass(password,user.getSlat()))){
            throw new GlobalException(RespBeanEnum.PASS_ERROR);
        }
        //生成cookie
        String ticket = UUIDUtil.uuid();
//        httpServletRequest.getSession().setAttribute(ticket,user);
        redisTemplate.opsForValue().set("user:"+ticket, user);
        CookieUtil.setCookie(httpServletRequest, httpServletResponse, "userTicket", ticket);
        return RespBean.success(ticket);
    }

    /**
     * 功能描述：在redis中根据cookie获取用户
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/17 21:28
     */
    @Override
    public User getUserByCookie(HttpServletRequest request, HttpServletResponse response,String ticket) {
        if(StringUtils.isEmpty(ticket)){
            return null;
        }
        User user = (User) redisTemplate.opsForValue().get("user:"+ticket);
//        if(user != null){
//            CookieUtil.setCookie(request, response, "userTicket", ticket);
//        }
        return user;
    }

    @Override
    public RespBean updatePassword(HttpServletRequest request, HttpServletResponse response,String ticket, String password) {
        User user = getUserByCookie(request, response, ticket);
        if(user == null){
            throw new GlobalException(RespBeanEnum.MOBILE_NOT_EXIST);
        }
        user.setPassword(MD5Util.inputPassToDBPass(password,user.getSlat()));
        int result = userMapper.updateById(user);
        if(result == 1){
            //删除redis缓存
            redisTemplate.delete("user:"+ticket);
            return RespBean.success();
        }
        return RespBean.error(RespBeanEnum.PASSWORD_UPDATE_FAIL);
    }
}
