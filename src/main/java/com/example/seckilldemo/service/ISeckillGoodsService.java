package com.example.seckilldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.seckilldemo.pojo.SeckillGoods;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
public interface ISeckillGoodsService extends IService<SeckillGoods> {

}
