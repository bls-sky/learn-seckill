package com.example.seckilldemo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.seckilldemo.pojo.User;
import com.example.seckilldemo.vo.LoginVo;
import com.example.seckilldemo.vo.RespBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sean
 * @since 2021-11-14
 */
public interface IUserService extends IService<User> {

    /**
     * 功能描述：登录
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/14 18:45
     */
    RespBean dologin(LoginVo loginVo, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse);

    /**
     * 功能描述：在redis中通过cookie查询用户对象
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/17 21:26
     */
    User getUserByCookie(HttpServletRequest request, HttpServletResponse response, String ticket);

    /**
     * 功能描述：用户修改密码
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/21 20:25
     */
    RespBean updatePassword(HttpServletRequest request, HttpServletResponse response,String ticket, String password);

}
