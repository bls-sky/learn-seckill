package com.example.seckilldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.seckilldemo.pojo.SeckillOrder;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
@Component
public interface SeckillOrderMapper extends BaseMapper<SeckillOrder> {

}
