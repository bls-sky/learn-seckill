package com.example.seckilldemo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.seckilldemo.pojo.Goods;
import com.example.seckilldemo.vo.GoodsVo;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sean
 * @since 2021-11-20
 */
@Component
public interface GoodsMapper extends BaseMapper<Goods> {

    /**
     * 功能描述：获取商品列表
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/20 15:23
     */
    List<GoodsVo> findGoodsVo();

    /**
     * 功能描述：通过商品id获取商品信息
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/20 16:27
     * @param goodsID
     */
    GoodsVo findGoodsVoByGoodsId(Long goodsID);
}
