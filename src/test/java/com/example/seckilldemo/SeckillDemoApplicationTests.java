package com.example.seckilldemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@SpringBootTest
class SeckillDemoApplicationTests {

    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    DefaultRedisScript defaultRedisScript;

    /**
     * 功能描述：使用redis的setIfAbsent操作来实现锁
     * 问题：如果在使用临界资源时，抛出异常，锁将永远不会被释放
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 21:27
     */
    @Test
    void testLock01() {
        ValueOperations valueOperations = redisTemplate.opsForValue();
        //lock存在的话，不可以set，也就是说不能获取到锁
        Boolean isLock = valueOperations.setIfAbsent("lock", "v1");
        //如果获取到锁
        if(isLock){
            valueOperations.set("name","sean");
            String name = (String) valueOperations.get("name");
            System.out.println(name);
            //操作结束，删除锁
            redisTemplate.delete("lock");
        }else{
            System.out.println("资源已经被锁住");
        }
    }

    /**
     * 功能描述：给锁加一个失效时间，解决所不会被释放的问题
     * 问题：失效时间设置5秒，但由于网络原因导致使用资源使用了10秒，那么十秒后又去删除锁，导致删除了别的线程加的锁
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 21:27
     */
    @Test
    void testLock02(){
        ValueOperations valueOperations = redisTemplate.opsForValue();
        //lock存在的话，不可以set，也就是说不能获取到锁
        Boolean isLock = valueOperations.setIfAbsent("lock", "v1",5, TimeUnit.SECONDS);
        //如果获取到锁
        if(isLock){
            valueOperations.set("name","sean");
            String name = (String) valueOperations.get("name");
            System.out.println(name);
            //抛出异常
            Integer.parseInt("xxxxx");//锁永远不能被释放
            //操作结束，删除锁
            redisTemplate.delete("lock");
        }else{
            System.out.println("资源已经被锁住");
        }
    }

    /**
     * 功能描述：给锁的value设置成随机值，删除的时候判断还是不是自己的锁，再用lua将 获取锁、判断锁、删除锁 三个操作包装成一个原子操作，可以安全删除锁
     *
     * @params:
     * @return：
     * @author: 59244
     * @time: 2021/11/27 21:27
     */
    @Test
    void testLock03(){
        ValueOperations valueOperations = redisTemplate.opsForValue();
        String value = UUID.randomUUID().toString();
        //lock存在的话，不可以set，也就是说不能获取到锁
        Boolean isLock = valueOperations.setIfAbsent("lock", value,50, TimeUnit.SECONDS);
        //如果获取到锁
        if(isLock){
            valueOperations.set("name","sean");
            String name = (String) valueOperations.get("name");
            System.out.println(name);
            System.out.println(valueOperations.get("lock"));
            Boolean result = (Boolean) redisTemplate.execute(defaultRedisScript, Collections.singletonList("lock"), value);
            System.out.println(result);
        }else{
            System.out.println("资源已经被锁住");
        }
    }

}
